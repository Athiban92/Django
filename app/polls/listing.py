
# coding: utf-8

import pygal
import random
import os
import pandas
import pandas.io.sql as sdb
import pyodbc
import matplotlib.pyplot as plt
from collections import OrderedDict
from pygal.style import DefaultStyle

class Listing_Chart():

    def __init__(self):


        # Data Getting From DB

        insertDatesql = '''SELECT    State,County, { fn MONTHNAME(Inserted_Date) } AS MonthName, YEAR(Inserted_Date) AS Year, count(distinct Property_URL) AS PropCount
                FROM         WebScraping_Complete_Data
                WHERE     [Account No] <> '' and (YEAR(Inserted_Date) = 2016) and State in ('FL','CO','CA','GA','NJ','NY','IL','IN','MD','VA','MA','NH','RI','DE','PA','OH','MN','NC','SC','MS','TN','OK') and county in ('Orange','Osceola','Seminole','Palm Beach','Jefferson','City and County of Broomfield','Elbert','Park','Clear Creek','Gilpin','Los Angeles','Riverside','Solano','Napa','Butts','Carroll','Cherokee','Coweta','Dawson','DeKalb','Fulton','Hall','Haralson','Heard','Henry','Jasper','Lamar','Newton','Pickens','Pike','Spalding','Walton','Camden','Burlington','Somerset','Gloucester','Atlantic','Hunterdon','Cape May','Salem','Bergen','Hudson','Kings','DeKalb ','Grundy','McHenry ','Lake','Porter','Prince Geroge"s','Arlington','Culpeper','Falls Church','Loudoun','Prince William','Fredericksburg','Warren ','Norfolk','Middlesex','Strafford','Worcester','Bristol','Kent','Newport','New Castle','Bucks','Montgomery','Brown','Isanti','Le Sueur','Washington','Wright','Mecklenburg','Rowan','Lancaster','York','DeSoto','Marshall','Tipton','Cheatham','Macon','Robertson','Rutherford','Smith','Trousdale','Williamson','Wilson','Grady')
                GROUP BY State,County, { fn MONTHNAME(Inserted_Date) }, MONTH(Inserted_Date), YEAR(Inserted_Date)
                order by Year(Inserted_Date),month(Inserted_Date)

                '''

        rerunDatesql = ''' SELECT    State,County, { fn MONTHNAME(Rerun_Date) } AS MonthName, YEAR(Rerun_Date) AS Year, count(distinct Property_URL) AS PropCount
                        FROM         WebScraping_Complete_Data
                        WHERE     [Account No] <> '' and (YEAR(Rerun_Date) = 2016) and State in ('FL','CO','CA','GA','NJ','NY','IL','IN','MD','VA','MA','NH','RI','DE','PA','OH','MN','NC','SC','MS','TN','OK') and county in ('Orange','Osceola','Seminole','Palm Beach','Jefferson','City and County of Broomfield','Elbert','Park','Clear Creek','Gilpin','Los Angeles','Riverside','Solano','Napa','Butts','Carroll','Cherokee','Coweta','Dawson','DeKalb','Fulton','Hall','Haralson','Heard','Henry','Jasper','Lamar','Newton','Pickens','Pike','Spalding','Walton','Camden','Burlington','Somerset','Gloucester','Atlantic','Hunterdon','Cape May','Salem','Bergen','Hudson','Kings','DeKalb ','Grundy','McHenry ','Lake','Porter','Prince Geroge"s','Arlington','Culpeper','Falls Church','Loudoun','Prince William','Fredericksburg','Warren ','Norfolk','Middlesex','Strafford','Worcester','Bristol','Kent','Newport','New Castle','Bucks','Montgomery','Brown','Isanti','Le Sueur','Washington','Wright','Mecklenburg','Rowan','Lancaster','York','DeSoto','Marshall','Tipton','Cheatham','Macon','Robertson','Rutherford','Smith','Trousdale','Williamson','Wilson','Grady')
                        GROUP BY State,County, { fn MONTHNAME(Rerun_Date) }, MONTH(Rerun_Date), YEAR(Rerun_Date)
                        order by Year(Rerun_Date),month(Rerun_Date)'''



        cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=HAWKIDEV;DATABASE=WebScraping_Master;UID=scraping;PWD=password')

      
        self.InsertDatedf = sdb.read_sql(insertDatesql, cnxn)

        self.RerunDatedf = sdb.read_sql(rerunDatesql, cnxn)


    def get_dict_from_frame(self,frame,state_name,year,month,name):


        if not str(frame.get(state_name)) == 'None':

            if not str(frame.get(state_name).get(int(year))) == 'None':

                if not str(frame.get(state_name).get(int(year)).get(month)) == 'None':
      
                    dict = frame[state_name][int(year)][month].to_dict()

                else:

                    print "The given month not in "+name

                    return {}
            else:

                print "The given year not in "+name
                return {}
        else:

            print "The given state not in "+name
            return {}

        return {k.capitalize():v for k,v in dict.items() }


    def main(self,state_name,month,year,type):


        groupInsert = self.InsertDatedf.groupby(['State','Year','MonthName','County'])['PropCount'].sum()

        insertkeysvalues = self.get_dict_from_frame(groupInsert,state_name,year,month,'Insert')

        groupRerun = self.RerunDatedf.groupby(['State','Year','MonthName','County'])['PropCount'].sum()

        rerunkeysvalues = self.get_dict_from_frame(groupRerun,state_name,year,month,'Rerun')

        if type == 'line':

            chart_type = pygal.Line

        else:

            chart_type = pygal.Bar

        line_chart = chart_type(print_values=True,print_values_position='top' ,style=DefaultStyle(
                      value_font_family='arial',
                      value_font_size=10,
                     ))

        # line_chart = pygal.Line()
        line_chart.title= state_name+" State -"+month+" "+year
        line_chart.x_title="County"
        line_chart.y_title="Total Properties"
        line_chart.x_labels = insertkeysvalues.keys()

        insertvalues = insertkeysvalues.values()

        for key,val in insertkeysvalues.items():

            if rerunkeysvalues.get(key):

                insertkeysvalues.update({key:rerunkeysvalues.get(key)})

            else:

                insertkeysvalues.update({key:0})

        rerunValues = insertkeysvalues.values()

        if not os.path.exists(os.getcwd()+"\\chartOutput"):
            os.mkdir(os.getcwd()+"\\chartOutput")

        line_chart.add('New Record',insertvalues)
        line_chart.add('Rerun Record',rerunValues)
        # line_chart.render_to_file(os.getcwd()+"\\chartOutput\\output_"+state_name+".svg")
        chart = line_chart.render_data_uri()
        print "ouput generated successfully!"
        return chart



