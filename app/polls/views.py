from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .listing import Listing_Chart
# from .sample import sample
def index(req):
	template = loader.get_template("index.html")
	return HttpResponse(template.render())


def output(req):
	template = loader.get_template("index.html")
	chart = Listing_Chart().main(req.GET.get('state'),req.GET.get('month'),req.GET.get('year'),req.GET.get('type'))
	context = {'data':chart}
	return HttpResponse(template.render(context))  